import ReactDOM from 'react-dom';
import { mount, route } from 'navi';
import { Router } from 'react-navi';

import App from './screens/App';
import Home from './screens/Home';
import Categories from './components/Dashboard/CategoriesPage/Categories';
import CreateCategory from './components/Dashboard/EditCategories/CreateCategory';
import EditCategories from './components/Dashboard/EditCategories/EditCategory';
import Clients from './components/Dashboard/Clients'
import Api from './common/Api';


import { withAuthentication} from './components/Auth/authenticatedRoute';
import { BOUNCE_IT_TOKEN_KEY } from './components/Auth/auth.api';



const routes = mount({
  "/login":route({
    title: 'Login',
    view: <App />
  }),
  "/": withAuthentication(route({
    title: 'Home',
    view: <Home />
  })
  ),
  "/editcategories": withAuthentication(route({
    title: 'EditCategories',
    view: <EditCategories />
  })
  ),
  "/clients": withAuthentication(route({
    title: 'InnerContent',
    view: <Clients />
  })
  ),
  "/categories": withAuthentication(route({
    title: 'Categories',
    view: <Categories />
  })
  ),
  "/createcategory": withAuthentication(route({
    title: 'CreateCategory',
    view: <CreateCategory />
  })
  )
})

Api.init();

ReactDOM.render(
  <Router 
  routes={routes}
  context = {{ token: localStorage.getItem(BOUNCE_IT_TOKEN_KEY)}}
  />,
  document.getElementById('root')
);

