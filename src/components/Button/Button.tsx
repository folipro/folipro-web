import styled from "styled-components";

type Props = { content: string; type: string; }

export default function Button({ content, type}: Props) {
    return (
        <StyledButton >
            {content}
        </StyledButton>
    )
}

const StyledButton = styled.button`
    background: linear-gradient(to right, #fee002 0%, #383838 90%);
    text-transform: uppercase;
    letter-spacing: 0.2 rem;
    width: 65%;
    height: 3rem;
    color: white;
    font-weight: bold;
    border: none;
    border-radius: 2rem;
    cursor: pointer;
    margin-top: 20px;
`;