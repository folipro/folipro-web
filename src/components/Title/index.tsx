import React from "react";

import { Container, TitleText } from "./styles";


const Navbar: React.FC = () => {
    return (
        <>
            <Container>
                 <TitleText>
                   CRM Folipro
                </TitleText>
                                               
            </Container>
        </>
    )
}

export default Navbar;