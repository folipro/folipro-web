import  Axios, {AxiosRequestConfig } from "axios";

export interface Credentials{
    email: string;
    password: string;
}

export const onLogin = async (data: Credentials) => {
    const requestConfig: AxiosRequestConfig ={
        method:'post',
        url: process.env.REACT_APP_API_BASE_URL + '/public/auth/login',
        data
    }
    try{
        const {data:response} = await Axios.request(requestConfig);
        storeToken(response)
        return{
           token: response
        }
    } catch (e: any){
        console.error(e);
        return {error: e.response.data.errors[0]}

    } 
}

export const BOUNCE_IT_TOKEN_KEY = 'bounce_it_token_key';

const storeToken = (token:string) => {
    localStorage.setItem(BOUNCE_IT_TOKEN_KEY, token)
}
