import styled from "styled-components";

export const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
height: 70px;
color: #fee002;
background-color: #383838;
border-radius: 20px;
margin-top: 5vh
`;


export const TitleText = styled.h2`
color: #fee002;
text-transform: uppercase;
letter-spacing:0.2rem;

`;