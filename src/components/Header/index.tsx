import React from "react";

import { Container, TitleText } from "./styles";


const Header: React.FC = () => {
    return (
        <>
            <Container>
                 <TitleText>
                   CRM Folipro
                </TitleText>                             
            </Container>
        </>
    )
}

export default Header;