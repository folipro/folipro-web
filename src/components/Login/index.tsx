import React, { useState } from "react";
import Input from "../Input/Input";
import { AuthForm } from "../Auth/AuthForm";
import {
    MainContainer,
    WelcomeText,
    InputContainer,
    ButtonContainer,
    ErrorMessage
} from "./styles";
import Button from "../Button/Button";
import { onLogin } from "../Auth/auth.api";
import { useNavigation } from "react-navi";

const Login: React.FC = () => {
    const navigation = useNavigation();
    const [{ email, password }, setCredentials] = useState({
        email: '',
        password: ''
    })

    const [error, setError] = useState('')

    const login = async (event: React.FormEvent) => {
        event.preventDefault();
        const { error, token } = await onLogin({
            email,
            password
        })

        if (error) {
            setError(error)
        } else {
            navigation.setContext({ token })
            navigation.navigate("/categories")
        }

    }


    return (
        <>
            <MainContainer>
                <WelcomeText>Bem-vindo(a)</WelcomeText>
                <AuthForm onSubmit={login}>
                    <InputContainer>
                        <Input type="text" placeholder="Usuário" value={email} onChange={(event) => setCredentials({
                            email: event,
                            password
                        })} />
                        <Input type="password" placeholder="Senha" value={password} onChange={(event) => setCredentials({
                            email,
                            password: event
                        })} />

                        <ButtonContainer >
                            <Button content="Entrar" type="submit" />
                        </ButtonContainer>
                        <ErrorMessage>
                            {error.length > 0 && <p>login ou senha incorreto(s)</p>}
                        </ErrorMessage>
                    </InputContainer>

                </AuthForm>

            </MainContainer>





        </>
    )
}

export default Login;