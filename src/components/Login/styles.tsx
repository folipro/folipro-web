import styled from "styled-components";

export const MainContainer = styled.div`

display: flex;
align-items: center;
flex-direction: column;
height: 50vh;
width: 40vh;
background: rgba(255,255,255, 0.15);
margin-top: 5vh;
backdrop-filter: blur(4.5px);
border-radius: 20px;
color: #ffffff;
text-transform: uppercase;
letter-spacing:0.2rem;
`;

export const WelcomeText = styled.h2`
margin: 3rem 0 2rem 0;
color: #fee002;
`;

export const InputContainer = styled.div`
display: flex;
flex-direction: column;
justify-content: space-around;
align-items: center;
height:20%;
width:100%;
margin-top: 85px;
`;

export const ButtonContainer = styled.div`
margin: 1rem 0 2rem;
display: flex;
justify-content: center;
align-items: center;
width:100%;
`;

export const ErrorMessage = styled.div`
height:20%;
width:100%;
margin-top: -10px;
text-transform: lowercase;
`;