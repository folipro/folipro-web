
import styled from "styled-components";

type Props = {
    onChange: (str: string) => void;
    type: string; 
    placeholder: string; 
    value: string, 
     
}

export default function Input({type, placeholder, value, onChange}:Props) {
    return <StyledInput type={type} 
    placeholder={placeholder} 
    value={value}
    onChange={event => onChange(event.target.value)}
    
    />;
    
}

const StyledInput = styled.input`
background: rgba(255, 255,255, 0.15);
box-shadow: 0 3px 20px 0 #fee002;
border-radius: 2rem;
width: 80%;
height: 3rem;
padding: 1rem;
border: none;
outline: none;
color: #fee002;
font-size: 1rem;
font-weight: bold;
margin-top: 20px;
&:focus{
    display: inline-block;
    box-shadow: 0 0 0 0.2rem #fee002;
    backdrop-filter: blur(12rem);
    border-radius: 2rem;
}
&::placeholder{
    color: #fee002;
    font-weight: 100;
    font-size: 1rem;
}


`