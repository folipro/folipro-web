import React from "react";
import { MenuButtom, TopbarStyle } from "./styles";
import Logo from "../../assets/logo.png"
import { MenuToggle } from "./styles";

interface Props {
    toggleVisibility: () => void;
}

const Topbar: React.FC<Props> = ({toggleVisibility}) => {
    return (
        <>
            <TopbarStyle>
                <MenuButtom onClick={toggleVisibility}>
                    <MenuToggle />
                </MenuButtom>
                <img alt="logo" src={Logo}/>
                <div></div>
            </TopbarStyle>
        </>
    )
}

export default Topbar;