import React from "react";
import {CloseCircleOutline} from '@styled-icons/evaicons-outline';
import {LogOutOutline} from '@styled-icons/evaicons-outline';
import { useNavigation } from "react-navi";
import { MenuWrapper, MenuTab, MenuLabel, MenuOverlay } from "./styles";



interface Props{
    isVisible:boolean;
    toggleVisibility: () => void;
}


const Menu: React.FC<Props> = ({isVisible, toggleVisibility}) =>{
    const navigation = useNavigation();
    const logOut = () =>{
        localStorage.clear();
        navigation.navigate("/login");
    }
    return (
    
        <>
           <MenuWrapper isVisible={isVisible}>
            <MenuTab >
                <MenuLabel onClick={toggleVisibility}>Fechar </MenuLabel>
                <CloseCircleOutline onClick={toggleVisibility} size="32"/>
            </MenuTab>
            <MenuTab >
                <MenuLabel onClick={logOut}>Logout </MenuLabel>
                <LogOutOutline onClick={logOut} size="36"/>
            </MenuTab>
           </MenuWrapper>
           <MenuOverlay onClick={toggleVisibility} isVisible={isVisible} />
        </>

)
} 

export default Menu;