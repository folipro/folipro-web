import styled from "styled-components";

interface isVisibleDiv{
    isVisible:boolean;
}

export const MenuWrapper = styled.div<isVisibleDiv>`
width:250px;
height: 100%;
position: fixed;
top:0;
left: 0;
background-color: #fee002;
z-index: 12;
transform: translateX(${props => props.isVisible ? 0 : '-100%'});
transition: transform 0.3s ease-out;
`

export const MenuOverlay = styled.div<isVisibleDiv>`
pointer-events: ${props => (props.isVisible ? 'all' : "none")};
position: fixed;
width:100vw;
height: 100%;
background-color: black;
opacity: 0.35;
top:0;
z-index: 11;
opacity: ${props => (props.isVisible ? 0.35 : 0)};
transition: opacity 0.3s ease-out;
`
export const MenuTab = styled.div`
display:flex;
justify-content: flex-end;
margin: 10% 10px;
align-items: center;
cursor: pointer;
`;

export const MenuLabel = styled.label`
text-transform: uppercase;
font-size: 22px;
font-weight: 550;
color: #383838;
cursor: pointer;
margin-right: 15px;
`

export const Container = styled.div`
display: flex;
justify-content: center;
align-items: center;
height: 100vh;
color: #fee002;
background-color: #383838;
`;


export const TopbarStyle = styled.header`
display:flex;
padding: 24px 32px;
align-items:center;
justify-content: space-between;
background-color: #383838;
color: #fee002;
`;

export const NavbarStyle = styled.nav`
display:flex;
width: 85%;
margin: 24px auto 16px;
justify-content: space-between;
`;

export const Separator = styled.hr`
width: 85%;
height: 7px;
margin: 0 auto;
border: none;
border-radius:16px;
background-color: #383838;
`;


export const CategoriesStyle = styled.div`
color: #fee002;
text-transform: uppercase;
letter-spacing:0.2rem;

`;

export const MenuButtom = styled.button`
background-color:transparent;
width: 3rem;
height: 3rem;

outline:none;
cursor: pointer;
border: none;
`;

export const MenuToggle = styled.span`
    display:block;
    width: 2rem;
    height: 2px;
    background-color: #fee002;
    position:relative;
    border-radius: 3px;
    &::after, &::before{
        width: 2rem;
        height: 2px;
        border-radius: 3px;
        background-color: #fee002;
        content: '';
        top:0;
        right: 0;
        position: absolute;
}
&::after{
    top:0.5rem;
}
&::before{
    top:-0.5rem;
}
`;

