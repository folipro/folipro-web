import React from "react";
import { NavbarStyle } from "./styles";
import {Link} from 'react-navi';
import './navbarstyle.css';
import { Separator } from "./styles";

const Navbar: React.FC =  () =>{
    return(
    <>
    <NavbarStyle>
        <Link activeClassName="active-link" className="link" href="/categories">Categorias</Link>
        <Link activeClassName="active-link" className="link" href="/editcategories">Editar categorias</Link>
        <Link activeClassName="active-link" className="link" href="/clients">Clientes</Link>
    </NavbarStyle>
    <Separator/>
    </>
    )
}

export default Navbar;