import React, { useState } from "react";

import Topbar from "./Topbar";
import Navbar from "./Navbar";
import Menu from "./Menu";



const Dashboard: React.FC = ({ children }) => {
    const [isVisible, setIsVisible] = useState(false);
    const toggleVisibility = () => setIsVisible(!isVisible);
    return (
        <>
            <Topbar toggleVisibility={toggleVisibility} />
            <Navbar />
            <Menu isVisible={isVisible} toggleVisibility={toggleVisibility}/>
            {children}
        </>
    )
}

export default Dashboard;