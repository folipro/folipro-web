import React from "react";
import EditCategory from "./EditCategory";

import styled from "styled-components";

const CreateCategoryPageWrapper = styled.div`
    margin-left: 170px;
    margin-top: 70px;

`

const Column = styled.div`
    flex:40%;
    flex-direction: column;
    display: flex;
`
const TwoColumnForm = styled.form`
display: flex;
${Column}:first-child{
    margin-right:-10%;
}
`;


const CreateCategory: React.FC = () => {
    return (      
            <EditCategory>
                 <CreateCategoryPageWrapper>
                     <header>Crie uma nova categoria</header>
                     <TwoColumnForm>
                        <Column> 1</Column>

                        <Column>2 </Column>
                     </TwoColumnForm>
                 </CreateCategoryPageWrapper>
            </EditCategory>
    )
}

export default CreateCategory;