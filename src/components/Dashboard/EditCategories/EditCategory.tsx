import React from "react";
import Dashboard from "../Dashboard";
import { Separator, NavbarStyle } from "../styles";
import { Link } from "react-navi";



const EditCategories: React.FC = ({ children }) => {
    return (
        <>
            <Dashboard>
                <NavbarStyle>
                    <Link activeClassName="active-link" className="link" href="/createcategory">Criar categoria</Link>
                    <Link activeClassName="active-link" className="link" href="">Atualizar categoria</Link>
                    <Link activeClassName="active-link" className="link" href="">Deletar categoria</Link>
                </NavbarStyle>

            </Dashboard>
            <Separator />
            {children}
        </>
    )
}

export default EditCategories;