import React from "react";
import { Category } from "../../../common/types";
import styled from "styled-components";

const CategoryWrapper = styled.button`   
    padding: 16px;
    align-items: center;
    background-color: #fee002ed;
    text-transform: capitalize;
    font-size: larger;
    font-weight: 500;
    color: black;
    min-width: 150px;
    text-align: center;
    text-decoration: none;
    padding: 16px;
    border-radius: 32px;
    cursor: pointer;
`

const CategoryColumns: React.FC<Category> = ({name}) =>(
<CategoryWrapper>
    {name} 
</CategoryWrapper>
)

export default CategoryColumns;