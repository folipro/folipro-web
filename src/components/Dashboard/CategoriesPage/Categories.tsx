import React, { useEffect, useState } from "react";
import Dashboard from "../Dashboard";
import { Link } from 'react-navi';
import {  Separator } from "../styles";
import { Category } from "../../../common/types";
import CategoryColumns from "./CategoryColumns";
import Api from "../../../common/Api";
import styled from "styled-components";

const categories =[
    {
        "id": 1,
        "name": "D+2",
        "color": "E9E9E9 (HEX color without #)",
        "daysToRememberValue": 2
      },
      {
        "id": 2,
        "name": "D+30",
        "color": "E9E9E9 (HEX color without #)",
        "daysToRememberValue": 30
      }, {
        "id": 3,
        "name": "D+60",
        "color": "E9E9E9 (HEX color without #)",
        "daysToRememberValue": 60
      }
]

const CategoriesList = styled.ul`
    list-style: none;
    padding-inline-end: 40px;
    display: flex;
    justify-content: space-around;
`

const Categories: React.FC = () => {

  /*   const [categories, setCategories] = useState<Category[]>([]);

    useEffect(() => {
        const fectchCategories = async () => {
            const { data } = await Api.get<Category[]>('category');
            setCategories(data);
        }
        fectchCategories();
    }, [])
 */
    return (
        <>
            <Dashboard>

                    
                    <CategoriesList >
                        {categories.map(category => <CategoryColumns {... category}/>)}
                    </CategoriesList>
                
                <Separator />
            </Dashboard>
        </>
    )
}

export default Categories;