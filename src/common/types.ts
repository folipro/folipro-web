export interface Category{
    id: number;
    name: string;
    color: string;
    daysToRememberValue: number;
}