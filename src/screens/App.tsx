import React from 'react';
import Title from '../components/Title';
import Login from '../components/Login';
import GlobalStyle from '../styles/global';

const App: React.FC = () => (
<>
    <GlobalStyle/>
    <Title />
    <Login />
</>
);

export default App;
