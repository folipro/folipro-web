import { createGlobalStyle} from "styled-components";
import BackgroundImage from '../assets/background1.png';



export default createGlobalStyle`
*{
    margin:200;
    outline:0;
    box-sizing: border-box;
    
}
#root{
    margin: 0 auto;
    
}

html {
    background: url(${BackgroundImage}) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size:cover;
    background-size: cover;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 50px;
    
    
}
`;